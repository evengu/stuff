# React nettavis


## Klon, last ned dependencies, og kjør
```sh
git clone https://gitlab.stud.idi.ntnu.no/evengu/stuff
cd MiniProjectV2/server
npm install
npm start
```
Åpne ny kommando-linje
```sh
cd MiniProjectV2/client
npm install 
npm start
```

Åpne nettavisen på [http://localhost:3000](http://localhost:3000)
