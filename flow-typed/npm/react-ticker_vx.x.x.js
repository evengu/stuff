// flow-typed signature: 46bd8ab8a8804873967c1e93250b0704
// flow-typed version: <<STUB>>/react-ticker_v^1.2.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-ticker'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-ticker' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'react-ticker/dist/index.es' {
  declare module.exports: any;
}

declare module 'react-ticker/dist' {
  declare module.exports: any;
}

// Filename aliases
declare module 'react-ticker/dist/index.es.js' {
  declare module.exports: $Exports<'react-ticker/dist/index.es'>;
}
declare module 'react-ticker/dist/index' {
  declare module.exports: $Exports<'react-ticker/dist'>;
}
declare module 'react-ticker/dist/index.js' {
  declare module.exports: $Exports<'react-ticker/dist'>;
}
