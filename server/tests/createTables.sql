drop table if exists comments;
drop table if exists articles;
drop table if exists categories;

create table articles(
    id int(11) primary key,
    author varchar(100) not null,
    title varchar(100) not null,
    imageUrl varchar(500),
    time varchar(30) not null,
    content varchar(5000) not null,
    category int(11) not null,
    important tinyint(1) not null
);

create table categories(
    id int(11) primary key,
    text varchar(30) not null
);

create table comments(
    id int(11) not null,
    author varchar(50) not null,
    content varchar(500) not null,
    articles_id int(11) not null,
    primary key(id, articles_id),
    foreign key (articles_id) references articles(id) on delete cascade
);

