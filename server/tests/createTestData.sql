insert into categories (id, text) VALUES
    (0, 'Nyheter'),
    (1, 'Sport'),
    (2, 'Kultur');

insert into articles (id, author, title, imageURL, time, content, category, important) VALUES
    (0, 'evengu', 'Title 1', null, '2019-11-21 09:24', 'This is the content of an important news article', 0, 1),
    (1, 'evengu', 'Title 2', null, '2019-11-21 09:25', 'This is the content of an important sports article', 1, 1),
    (2, 'evengu', 'Title 3', null, '2019-11-21 09:26', 'This is the content of an important culture article', 2, 1),
    (3, 'evengu', 'Title 4', null, '2019-11-21 09:27', 'This is the content of an unimportant news article', 0, 0),
    (4, 'evengu', 'Title 5', null, '2019-11-21 09:28', 'This is the content of an unimportant sports article', 1, 0),
    (5, 'evengu', 'Title 6', null, '2019-11-21 09:29', 'This is the content of an unimportant culture article', 2, 0);

insert into comments (id, author, content, articles_id) VALUES
    (0, 'evengu', 'Comment 0:0', 0),
    (1, 'evengu', 'Comment 0:1', 0),
    (0, 'evengu', 'Comment 1:0', 1),
    (0, 'evengu', 'Comment 2:0', 2),
    (1, 'evengu', 'Comment 2:1', 2),
    (0, 'evengu', 'Comment 3:0', 3),
    (0, 'evengu', 'Comment 5:0', 5);
