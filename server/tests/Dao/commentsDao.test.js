let mysql = require('mysql');

const CommentsDao = require('../../src/Dao/commentsDao');
const runsqlfile = require("../runsqlfile.js");

//GitLab pool
let pool = mysql.createPool({
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "secret",
    database: "supertestdb",
    debug: false,
    multipleStatements: true
});


let commentsDao = new CommentsDao(pool);

beforeAll(done => {
    runsqlfile("tests/createTables.sql", pool, () => {
        runsqlfile("tests/createTestData.sql", pool, done);
    });
});

afterAll(() => {
    pool.end();
});

function printCallback(status, data){
    console.log("Test callback: status=" + status + ", data=" + JSON.stringify(data));
}

test("Get all comments related to one article in the database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.length).toBe(2);
        expect(data[0].content).toBe('Comment 0:0');
        done();
    }
    commentsDao.getAll(0, callback);
});

test("Create one comment in the database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }
    let json: any = {
        id: 2,
        author: 'evengu',
        content: 'Comment 0:2',
        articles_id: 0
    };
    commentsDao.createOne(json, callback);
});

test("Delete one comment in the database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.affectedRows).toBe(1);
        done();
    }
    let json = {
        id: 0,
        articles_id: 2
    };
    commentsDao.deleteOne(json, callback);
});
