let mysql = require('mysql');

const ArticleDao = require('../../src/Dao/articleDao');
const runsqlfile = require("../runsqlfile.js");

//GitLab pool
let pool = mysql.createPool({
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "secret",
    database: "supertestdb",
    debug: false,
    multipleStatements: true
});

let articleDao = new ArticleDao(pool);

beforeAll(done => {
   runsqlfile("tests/createTables.sql", pool, () => {
       runsqlfile("tests/createTestData.sql", pool, done);
   });
});

afterAll(() => {
    pool.end();
});

function printCallback(status, data){
    console.log("Test callback: status=" + status + ", data=" + JSON.stringify(data));
}

test("Get one article from database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.length).toBe(1);
        expect(data[0].title).toBe('Title 1');
        done();
    }
    articleDao.getOne(0, callback);
});

test("Get non-existing article from database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.length).toBe(0);
        done();
    }
    articleDao.getOne(99, callback);
});

test("Get all articles from database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.length).toBe(6);
        done();
    }
    articleDao.getAll(callback);
});

test("Create one article in database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }
    let json = {
        id: 6,
        author: 'evengu',
        date: '2019-11-21 09:39',
        category: 1,
        important: 0,
        headline: 'Title 7',
        imageURL: null,
        content: 'This is a created article'
    };
    articleDao.createOne(json, callback);
});

test("Edit an article in the database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }
    let json = {
        author: 'evengu',
        time: '2019-11-21 09:42',
        category: 1,
        important: 0,
        title: 'Title 7',
        imageURL: null,
        content: 'This is an edited article'
    };
    articleDao.editOne(json, 6, callback);
});

test("Delete an article", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.affectedRows).toBe(1);
        done();
    }
    articleDao.deleteOne(6, callback);
});