let mysql = require('mysql');

const MiscDao = require('../../src/Dao/miscDao');
const runsqlfile = require("../runsqlfile.js");

//GitLab pool
let pool = mysql.createPool({
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "secret",
    database: "supertestdb",
    debug: false,
    multipleStatements: true
});

let miscDao = new MiscDao(pool);

beforeAll(done => {
    runsqlfile("tests/createTables.sql", pool, () => {
        runsqlfile("tests/createTestData.sql", pool, done);
    });
});

afterAll(() => {
    pool.end();
});

function printCallback(status, data){
    console.log("Test callback: status=" + status + ", data=" + JSON.stringify(data));
}

test("Get all IDS from database", done => {
   function callback(status, data){
        printCallback(status, data);
        expect(data.length).toBe(6);
        expect(data[0].id).toBe(0);
        done();
   }
   miscDao.getAllIds(callback);
});

test("Get all categories from database", done => {
    function callback(status, data){
        printCallback(status, data);
        expect(data.length).toBe(3);
        expect(data[0].text).toBe('Nyheter');
        expect(data[0].id).toBe(0);
        done();
    }
    miscDao.getCategories(callback);
});