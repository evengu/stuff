//@flow
const Dao = require('./dao.js');

module.exports = class MiscDao extends Dao{
    getCategories(callback: function){
        super.query("select * from categories", [], callback);
    }

    getAllIds(callback: function){
        super.query("select id from articles", [], callback);
    }
};