//@flow
const Dao = require('./dao');

module.exports = class ArticleDao extends Dao{
    getAll(callback: function){
        super.query("select * from articles", [], callback);
    }

    getOne(id: number, callback): function{
        super.query("select * from articles where id = ?", [id], callback);
    }

    createOne(json: {id: number, author: string, date: string, category: number, important: boolean, headline: string, imageURL: string, content: string}, callback: function){
        let val = [json.id, json.author, json.date, json.category, json.important, json.headline, json.imageURL, json.content];
        super.query("insert into articles (id, author, time, category, important, title, imageURL, content) values (?,?,?,?,?,?,?,?)",
            val, callback
        );
    }

    editOne(json: {author: string, title: string, imageURL: string, time: string, content: string, category: number, important: boolean}, id: number, callback: function){
        let val = [json.author, json.title, json.imageURL, json.time, json.content, json.category, json.important, id];
        super.query("update articles set author=?, title=?, imageURL=?,time=?,content=?,category=?,important=? where id=?",
            val, callback)
    };

    deleteOne(id: number, callback: function){
        super.query("delete from articles where id = ?", [id], callback);
    }
};