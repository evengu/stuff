//@flow
module.exports = class Dao {
    constructor(pool: any){
        this.pool = pool;
    }

    query(sql: string, params: any, callback: function){
        this.pool.getConnection((err: any, connection: any) => {
            console.log("Connecting to Database");
            if (err){
                console.log("Error connecting to Database");
                callback(500, {error: "Error connecting to database"});
            }
            else{
                console.log("Running SQL: " + sql);
                connection.query(sql, params, (err: any, rows: any) => {
                    connection.release();
                    if(err){
                        console.log(err);
                        callback(500, {error: "Error querying"});
                    }
                    else{
                        console.log("Returning rows");
                        callback(200, rows);
                    }
                })
            }
        })
    }
};