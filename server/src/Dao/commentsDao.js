//@flow

const Dao = require('./dao');

module.exports = class CommentsDao extends Dao{
    getAll(id: number, callback: function){
        super.query("select * from comments where articles_id=?", [id], callback);
    }

    createOne(json: {id: number, author: string, content: string, articles_id: number}, callback: function){
        let val = [json.id, json.author, json.content, json.articles_id];
        super.query("insert into comments values (?, ?, ?, ?)",
            val, callback
        );
    }

    deleteOne(json: {id: number, articles_id: number}, callback: function){
        let val = [json.id, json.articles_id];
        super.query("delete from comments where id = ? and articles_id = ?", val, callback);
    }
};