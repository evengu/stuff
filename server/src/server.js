//@flow

let express = require("express");
let mysql = require("mysql");
let bodyParser = require("body-parser");

const ArticleDao = require('./Dao/articleDao.js');
const CommentsDao = require('./Dao/commentsDao.js');
const MiscDao = require('./Dao/miscDao.js');

let app: any = express();
app.use(bodyParser.json());

let pool: any = mysql.createPool({
        connectionLimit: 2,
        host: "mysql.stud.iie.ntnu.no",
        user: "evengu",
        password: "O7KhlwWQ",
        database: "evengu",
        debug: false
    }
);

let articleDao = new ArticleDao(pool);
let commentsDao = new CommentsDao(pool);
let miscDao = new MiscDao(pool);

//Hent alle artikler
app.get("/article", (req: any, res: any) => {
    articleDao.getAll((status, data) => {
        res.status(status);
        res.json(data);
    })
});

//hent en artikkel
app.get("/article/:article_id", (req, res) => {
    articleDao.getOne(req.params.article_id, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//Legg til en artikkel
app.post("/article", (req, res) => {
    articleDao.createOne(req.body, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//Slett en artikkel
app.delete("/article", (req, res) => {
    articleDao.deleteOne(req.body.id, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//Rediger en artikkel
app.put("/article/edit/:article_id", (req, res) => {
    articleDao.editOne(req.body, req.params.article_id, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//Hent alle kommentarer koblet til en artikkel
app.get("/comments/:article_id", (req, res) => {
    commentsDao.getAll(req.params.article_id, (status, data) => {
        res.status(status);
        res.json(data);
    })
});

//Legg til en kommentar på en artikkel
app.post("/comment", (req, res) => {
    commentsDao.createOne(req.body, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//Slett en kommentar på en artikkel
app.delete("/comment", (req, res) => {
    console.log(req.body);
    commentsDao.deleteOne(req.body, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//Hent alle id'er
app.get("/ids", (req, res) => {
   miscDao.getAllIds((status, data) => {
       res.status(status);
       res.json(data);
   })
});

app.get("/categories", (req, res) => {
    console.log("Hello World!");
    miscDao.getCategories((status, data) => {
        res.status(status);
        res.json(data);
    })
});

app.listen(4000, () => console.log('Server is running'));

/*


 */