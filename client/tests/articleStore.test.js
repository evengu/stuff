import {Article} from '../src/Article';
import {Comment} from "../src/Comment";

test("Checking Comment() constructor", done => {
   let comment = new Comment(0, "evengu", "Some content", 0);
   expect(comment.id).toBe(0);
   expect(comment.author).toBe("evengu");
   expect(comment.articles_id).toBe(0);
   done();
});

test("Checking Article constructor", done => {
    let article = new Article(0, "evengu", "title", null, "time", "content", 1, true);
    expect(article.imageURL).toBe(Article.constantURL);
    done();
});
