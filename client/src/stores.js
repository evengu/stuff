//@flow
import axios from 'axios';
import {sharedComponentData} from "react-simplified";
import {history} from "./myWidgets";
import {Article} from './Article';
import {Comment} from './Comment';

const header = {
  "Content-Type": "application/json"
};

class ArticleStore {

  articles: Article[] = [];
  currentArticle: Article = null;
  categories: any = {};

  assignArticle(article: Article){
    this.currentArticle.id = article.id;
    this.currentArticle.title = article.title;
    this.currentArticle.content = article.content;
    this.currentArticle.imageURL = article.imageURL;
    this.currentArticle.time = article.time;
    this.currentArticle.comments = article.comments;
    this.currentArticle.category = article.category;
    this.currentArticle.important = article.important;
  }

  getIds(){
    return axios.get("http://localhost:4000/ids").then(response =>  {
      if (response.status === 500){
        history.push("/server-error");
        return null;
      }
      else{
        console.log(response.data);
        return response.data;
      }
    }).then(data => data.map(element => element.id))
        .catch(error => {
          history.push("/server-error");
          console.log(error);
        });
  }

  getCategories(){
    return axios.get('http://localhost:4000/categories').then(response => {
      if (response.status === 500){
        history.push("/server-error");
        this.categories = null;
        return null;
      }
      this.categories = response.data;
      return this.categories;
    }).catch(error => {
      history.push("/server-error");
      console.log(error);
    });
  }

  createArticle(response: any): Article{
    return new Article(response.id,
        response.author, response.title, response.imageURL, response.time, response.content, response.category,
        response.important === 1);
  }

  getArticle(id: number): Article{
    return axios.get('http://localhost:4000/article/' + id).then(response => {
      if (response.status === 500){
        history.push("/server-error");
        this.currentArticle = null;
        return null;
      }
      if (response.data[0]){
        this.currentArticle = this.createArticle(response.data[0]);
        let article: Article = this.articles.find(a => this.currentArticle.id === a.id);
        if (article){
          this.assignArticle(article);
        }
      }
      else{
        this.currentArticle = null;
        history.push("/server-error");
        console.log(id + ' not found');
      }
      return this.currentArticle;
    }).then(() => {
      if (this.currentArticle !== null){
        this.getComments(id).then(res => this.currentArticle.addComments(res));
      }
    })
        .catch(error => {
          history.push("/server-error");
          console.log(error);
        });
  }

  getAllArticles(): Article[]{
    return axios.get('http://localhost:4000/article').then(response => {
      if (response.status === 500){
        this.articles = null;
        history.push("/server-error");
        return null;
      }
      this.articles = response.data.map(responsePart => this.createArticle(responsePart));
      return this.articles;
    }).catch(error => {
          history.push("/server-error");
          console.log(error);
    });
  }

  deleteCurrentArticle(){
    console.log('Deleting ' + this.currentArticle.id);
    axios.delete('http://localhost:4000/article',
      {
        headers: header,
          data: JSON.stringify({id: this.currentArticle.id})
      }).then(response => {
        if (response.status === 500){
          this.currentArticle = null;
          history.push("/server-error");
        }
        else{
          this.articles = this.articles.filter(article => (article.id !== this.currentArticle.id));
          this.currentArticle = null;
          history.push('/')
        }
    }).catch(error => {
      history.push("/server-error");
      console.log(error);
    });
  }

  getComments(id: number): Comment[]{
    return axios.get('http://localhost:4000/comments/' + id,
        {
          headers: header,
          data: JSON.stringify({article_id: id})
        }).then(response => {

          if (response.status === 500){
            history.push("/server-error");
            return null;
          }

          if (response.data.length > 0){
            return response.data.map(commentData => new Comment(commentData.id, commentData.author, commentData.content));
          }
          return undefined;
    }).catch(error => {
      history.push("/server-error");
      console.log(error);
    });
  }

  postArticle(article: Article): any{

    let data: {id: number, author: string, date: string, category: number, important: boolean, headline: string, imageURL: string, content: string} = {
      id: article.id,
      author: article.author,
      date: article.time,
      category: article.category,
      important: article.important,
      headline: article.title,
      imageURL: article.imageURL,
      content: article.content
    };

    return axios.post('http://localhost:4000/article', JSON.stringify(data), {headers: header})
        .then(res => {
          if (res.status === 500){
            history.push("/server-error");
            return null;
          }
          else{
            return res;
          }
        })
        .catch(error => {
          history.push("/server-error");
          console.log(error);
        });
  }

  editArticle(article: Article){
    this.currentArticle.author = article.author;
    this.currentArticle.time = article.time;
    this.currentArticle.category = article.category;
    this.currentArticle.important = article.important;
    this.currentArticle.title = article.title;
    this.currentArticle.imageURL = article.imageURL;
    this.currentArticle.content = article.content;

    let articleFromList: Article = this.articles.find(a => this.currentArticle.id === a.id);
    if (articleFromList){
      this.assignArticle(articleFromList);
    }

    let data: {author: string, title: string, imageURL: string, time: string, content: string, category: number, important: boolean} = {
      author: article.author,
      title: article.title,
      imageURL: article.imageURL,
      time: article.time,
      content: article.content,
      category: article.category,
      important: article.important,
    };

    return axios.put('http://localhost:4000/article/edit/' + article.id,
        JSON.stringify(data), {headers: header})
        .then(res => {
          if (res.status === 500){
            history.push("/server-error");
            return null;
          }
          else{
            return res;
          }
        })
        .catch(error => {
          history.push("/server-error");
          console.log(error);
        });

  }

  postComment(comment: Comment){

    let data: {id: number, author: string, content: string, articles_id: number} = {
      id: comment.id,
      author: comment.author,
      content: comment.content,
      articles_id: comment.articles_id
    };

    return axios.post('http://localhost:4000/comment', JSON.stringify(data), {headers: header})
      .then(res => {
        if (res.status === 500){
          history.push("/server-error");
          return null;
        }
        else{
          return res;
        }
      }).catch(error => {
        history.push("/server-error");
            console.log(error);
      });
  }

  deleteComment(id: number){
    let data: {id: number, articles_id: number} = {
      id: id,
      articles_id: this.currentArticle.id
    };
    return axios.delete('http://localhost:4000/comment', {headers: header, data: JSON.stringify(data)})
        .then(res => {
          if (res.status === 500){
            history.push("/server-error");
            return null;
          }
          else{
            return res;
          }
        })
        .catch(error => {
          history.push("/server-error");
          console.log(error);
        });
  }
}

export let articleStore = sharedComponentData(new ArticleStore());