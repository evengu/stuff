//@flow
import React from 'react';
import ReactDOM from 'react-dom';
import {Category, FindError, Home, ServerError} from './myWidgets';
import {HashRouter, Route, Switch} from "react-router-dom";
import {articleStore} from "./stores";
import {AddArticle, EditArticle, SingleArticle} from "./ArticleWidgets";
import {history} from "./myWidgets";
import {Alert} from "./widgets";

const root: any = document.getElementById('root');
if (root)
    articleStore.getCategories().then(res => {
        if (res.status === 500){
            ReactDOM.render(
                <HashRouter>
                    <div>
                        <Route path="/" component={ServerError}/>
                    </div>
                </HashRouter>,
                root
            );
            history.push("/server-error");
        }
        else{
            ReactDOM.render(
                <HashRouter>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/articles" component={Home}/>
                        <Route exact path="/articles/article" component={Home}/>
                        {
                            res.map(c => <Route key={c.id} exact path={"/articles/" + c.text} render={(props) => <Category {...props} category={c.id}/>}/>)
                        }
                        <Route exact path="/articles/article/:id" component={SingleArticle}/>
                        <Route exact path="/articles/create" component={AddArticle}/>
                        <Route exact path="/articles/edit/:article_id" component={EditArticle}/>
                        <Route exact path="/server-error" component={ServerError}/>
                        <Route component={FindError}/>
                    </Switch>
                </HashRouter>,
                root
            );
        }
    }).catch(error => {
        ReactDOM.render(
            <HashRouter>
                <div>
                    <Route path="/" component={ServerError}/>
                </div>
            </HashRouter>,
            root
        );
        history.push("/server-error");
        console.log(error);
    });


