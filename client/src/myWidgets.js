//@flow
import {Component} from "react-simplified";
import {articleStore} from "./stores";
import {NavLink} from "react-router-dom";
import * as React from "react";
import {createHashHistory} from "history";
import {ArticleGrid} from "./ArticleWidgets";
import {Card} from "./widgets";

const Article = require('../src/Article');

export const history = createHashHistory();

export class Header extends Component {
    render() {
        return (
            <nav className='navbar navbar-expand-sm bg-light navbar-light'>
                <NavLink className='navbar-brand' activeClassName='active' exact to='/'><img src={Article.constantURL}
                                                                                             alt='koalalogo'
                                                                                             style={{width: '50px'}}/></NavLink>
                <div className='navbar-nav w-100'>
                    <NavLink className='nav-link text-info categoryLinks' activeClassName='active' exact to='/'>Hjem</NavLink>
                    <CategoryLinks/>
                </div>
                <div className='navbar-nav w-25'>
                    <NavLink className='nav-link text-info categoryLinks' activeClassName='active' exact to='/articles/create'>Create
                        Article</NavLink>
                </div>
            </nav>
        );
    }

}

export class Home extends Component{
    render(){
        return(
            <div>
                <Header/>
                <Ticker/>
                <ArticleGrid/>
            </div>
        );
    }
}

export class Category extends Component<{category: number}>{
    render(){
        return(
            <div>
                <Header/>
                <ArticleGrid category={this.props.category}/>
            </div>
        );
    }
}

class CategoryLinks extends Component{
       render(){
        if (articleStore.categories){
            return (
                <div>
                    {
                        articleStore.categories.map(c => <NavLink key={c.id} className='nav-link text-info categoryLinks' activeClassName='active' exact
                                                                  to={'/articles/' + c.text}>{c.text}</NavLink>)
                    }
                </div>
            );
        }
        return null;
    }

    mounted(){
        articleStore.getCategories().catch(error => console.log(error));
    }
}

export class Ticker extends Component {

    checkTime(input: string) {
        let writtenDate = input.split(" ")[0];
        let currentDate = new Date();
        let writtenYear = writtenDate.split("-")[0];
        let writtenMonth = writtenDate.split("-")[1];
        return (writtenYear * 1 === currentDate.getFullYear() && writtenMonth * 1 === currentDate.getMonth() + 1);
    }

    render() {
        return (
            <div className='ticker-wrap card container'>
                <div className='ticker'>
                    {
                        articleStore.articles.filter(e => this.checkTime(e.time)).map(e => (
                            <NavLink className='text-info ticker-item' exact to={'/articles/article/' + e.id}
                                     key={e.id}>
                                {e.title} &nbsp; {e.time}
                            </NavLink>
                        ))
                    }
                </div>
            </div>
        );
    }

    mounted() {
        articleStore.getAllArticles().catch(error => console.log(error));
    }
}

export class ServerError extends Component{
    render(){
        return(
            <div>
                <Card title="Server error 500">
                    <p>Beklager forstyrrelsen, vi jobber med saken. <NavLink exact to="/" className="nav-link text-info">Her er en link tilbake til hovedsiden.</NavLink></p>
                    <p>Om problemet fortsetter, vennligst prøv igjen senere.</p>
                </Card>
            </div>
        );
    }
}

export class FindError extends Component{
    render(){
        return(
            <div>
                <Card title="Error 404: Cannot find page">
                    <p>Vi beklager, men denne siden finnes ikke. <NavLink exact to="/" className="nav-link text-info">Her er en link tilbake til hovedsiden.</NavLink></p>
                </Card>
            </div>
        );
    }
}