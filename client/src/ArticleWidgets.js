//@flow

import {Component} from "react-simplified";
import {articleStore} from "./stores";
import {NavLink} from "react-router-dom";
import {Alert, Button, Card, Column, Row} from "./widgets";
import Image from "react-bootstrap/Image";
import * as React from "react";
import {Header} from "./myWidgets";
import {AddComment, SingleComment} from "./CommentWidgets";
import {history} from "./myWidgets";

const Article = require('../src/Article');

export class ArticleGrid extends Component<{ category?: number }> {

    mounted() {
        articleStore.getAllArticles();
    }

    render() {
        if (articleStore.articles != null) {
            return (
                <div className='container' key={1}>
                    {
                        articleStore.articles.filter(a => ((this.props.category === undefined && a.important) || (a.category === this.props.category))).map(article => (
                                <div className='container m-4 article-box' key={article.id}>
                                    <NavLink className='card-link' exact to={'/articles/article/' + article.id}>
                                        <Card title={article.title}>
                                            <Image alt={article.title} src={article.imageURL} style={{width: '200px'}}
                                                   fluid/>
                                        </Card>
                                    </NavLink>
                                </div>
                            )
                        )
                    }
                </div>
            );
        } else {
            return null;
        }
    }
}

export class SingleArticle extends Component<{ match: { params: { id: number } } }> {
    constructor(props: any) {
        super(props);
        if (this.props.match != null && this.props.match.params.id) {
            console.log("Fetching " + this.props.match.params.id);
            articleStore.getArticle(this.props.match.params.id);
        }
    }

    render() {
        if (articleStore.currentArticle != null) {
            return (
                <div>
                    <Header/>
                    <Card title={articleStore.currentArticle.title}>
                        <Image className='singleArticleImage' alt={articleStore.currentArticle.title} src={articleStore.currentArticle.imageURL}
                               style={{width: '200px'}} fluid/>
                        <p>{articleStore.currentArticle.content}</p>
                        <Row>
                            <Column><Button.Info
                                onClick={() => (history.push("/articles/edit/" + articleStore.currentArticle.id))}>Edit this article</Button.Info></Column>
                            <Column><Button.Danger
                                onClick={() => articleStore.deleteCurrentArticle()}>Delete this article</Button.Danger></Column>
                        </Row>
                    </Card>
                    <Card title="Comments">
                        {
                            articleStore.currentArticle.comments.map(comment => <SingleComment key={comment.id}
                                                                                               comment={comment}/>)
                        }
                    </Card>
                    <Alert/>
                    <AddComment article_id={articleStore.currentArticle.id}/>
                </div>
            );
        } else {
            return null;
        }
    }
}

class AddOrEditArticle extends Component {
    author: string;
    title: string;
    imageURL: string;
    imageError: string = "";
    content: string;
    category: number;
    important: boolean;
    time: string;
    authorRef: any;
    titleRef: any;
    imageRef: any;
    contentRef: any;
    categoryRef: any;
    importantRef: any;

    errors: string[] = [];

    validateURL(URL: string) {
        return (URL.match(/\.(jpeg|jpg|gif|png)$/) != null);
    }

    constructor(props: any) {
        super(props);
        this.authorRef = React.createRef();
        this.titleRef = React.createRef();
        this.imageRef = React.createRef();
        this.contentRef = React.createRef();
        this.categoryRef = React.createRef();
        this.importantRef = React.createRef();
    }

    render() {
        return (
            <div>
                <Header/>
                <Alert/>
                <Card title='Add article' id='title'>
                    <form>
                        <Row>
                            <Column width={2}>
                                <span className='input-group-text'>Brukernavn</span>
                            </Column>
                            <Column width={8}>
                                <input type='text' id='authorValue' ref={this.authorRef}
                                       value={this.author}
                                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                           this.author = event.target.value
                                       }}
                                       className="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" required/>
                            </Column>
                            <Column>*</Column>
                        </Row>
                        <Row>
                            <Column width={2}>
                                <span className='input-group-text'>Overskrift</span>
                            </Column>
                            <Column width={8}>
                                <input type='text' id='titleValue' ref={this.titleRef}
                                       value={this.title}
                                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                           this.title = event.target.value
                                       }}
                                       className="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" required/>
                            </Column>
                            <Column>*</Column>
                        </Row>
                        <Row>
                            <Column width={2}>
                                <span className='input-group-text'>Bilde (Bare lenke)</span>
                            </Column>
                            <Column width={8}>
                                <input type="text" className='form-control' id="imageURL" ref={this.imageRef}
                                       onChange={event => {
                                           if (event.target.value.trim() === ""){
                                               this.imageError = "";
                                               this.imageURL = null;
                                           }
                                           else if (this.validateURL(event.target.value)) {
                                               console.log("Validate");
                                               this.imageURL = event.target.value;
                                               this.imageError = "";
                                           } else {
                                               this.imageError = "Not an image";
                                               this.imageURL = null;
                                           }
                                       }} pattern=".(jpeg|jpg|gif|png)"/>
                            </Column>
                        </Row>
                        <Row>
                            <Column width={2}>
                                <div className='input-group-prepend'>
                                    <span className='input-group-text'>Innhold</span>
                                </div>
                            </Column>
                            <Column width={8}>
                                <textarea id='contentValue' ref={this.contentRef} value={this.content} className='form-control'
                                aria-label="With textarea" rows={15}
                                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                    this.content = event.target.value
                                }} required/>
                            </Column>
                            <Column>*</Column>
                        </Row>
                        <Row>
                            <Column width={2}>
                                <span className="input-group-text">Kategori</span>
                            </Column>
                            <Column width={8}>
                                <select id='categorySelection' ref={this.categoryRef} defaultValue={0}
                                        className="custom-select"
                                        onChange={event => {
                                            this.category = event.target.options[event.target.selectedIndex].value
                                        }}>
                                    {
                                        articleStore.categories.map(c => <option value={c.id} key={c.id}>{c.text}</option>)
                                    }
                                </select>
                            </Column>
                            <Column>*</Column>
                        </Row>
                        <Row>
                            <Column width={2}>
                                <span className="input-group-text">Hovedsideverdig?</span>
                            </Column>
                            <Column width={8}>
                                    <input id='importantValue' className="custom-checkbox" ref={this.importantRef} type='checkbox' onClick={event => {
                                        this.important = event.target.checked
                                    }}/>
                            </Column>
                            <Column>*</Column>
                        </Row>
                        <Row>
                            <Column width={10}>Alle felter merket med * er nødvendige</Column>
                        </Row>
                        <button className="btn btn-outline-secondary" type="button"
                                onClick={this.submitArticle}>Submit
                        </button>
                    </form>
                </Card>
            </div>
        );
    }

    updateInternalValues() {
        this.author = this.authorRef.current.value;
        this.title = this.titleRef.current.value;
        this.imageURL = (this.imageRef.current.value.trim().length > 0 && this.validateURL(this.imageRef.current.value) ? this.imageRef.current.value : null);
        this.content = this.contentRef.current.value;
        this.important = this.importantRef.current.checked;
        let categorySelection = this.categoryRef.current;
        this.category = categorySelection.options[categorySelection.selectedIndex].value;
    }

    updateInputValues(author: string, title: string, imageURL: string, content: string, important: boolean, category: number) {
        this.authorRef.current.value = author;
        this.titleRef.current.value = title;
        this.imageRef.current.value = (imageURL === Article.constantURL ? null : imageURL);
        this.contentRef.current.value = content;
        this.importantRef.current.checked = important;
        this.categoryRef.current.value = category;
        this.updateInternalValues();
    }

    createArticle(id: number) {
        let today = new Date();
        this.time = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + " " +
            (today.getHours() > 9 ? today.getHours() : '0' + today.getHours()) + ":" +
            (today.getMinutes() > 9 ? today.getMinutes() : '0' + today.getMinutes());
        console.log(this.imageURL);
        return new Article(id, this.author, this.title, this.imageURL, this.time, this.content, this.category, this.important);
    }

    submitArticle() {
        let errors: string[] = [];
        if (this.authorRef.current.value.length === 0){
            errors.push("Husk brukernavn");
        }
        if (this.titleRef.current.value.length === 0){
            errors.push("Husk overskrift");
        }
        if (this.imageRef.current.value.length > 0 &&  !(this.validateURL(this.imageRef.current.value))){
            errors.push("Bildet er feil");
        }
        if (this.contentRef.current.value.length === 0){
            errors.push("Husk innhold");
        }

        if (errors.length > 0){
            let error = errors.reduce((acc, currenValue) => (acc + currenValue + " | "), "");
            Alert.danger(error);
            return false;
        }
        else{
            return true;
        }
    }
}

export class AddArticle extends AddOrEditArticle {
    submitArticle() {
        if (super.submitArticle()){
            articleStore.getIds().then(ids => {
                console.log(ids);
                let id: number;
                if (ids.length === 0) {
                    id = 0;
                } else {
                    id = Math.max(...ids) + 1;
                }

                super.updateInternalValues();

                console.log(id);

                let newArticle = super.createArticle(id);

                console.log(newArticle);

                articleStore.articles.push(newArticle);

                articleStore.postArticle(newArticle).then(response => {
                    if (response.statusText === 'OK') {
                        history.push("/articles/article/" + newArticle.id);
                    }
                }).catch(error => console.log("Error:" + error));
            });
        }
        return true;
    }
}

export class EditArticle extends AddOrEditArticle<{ match: { params: { article_id: number } } }> {

    componentDidMount() {
        articleStore.getArticle(this.props.match.params.article_id).then(() => {
            super.updateInputValues(articleStore.currentArticle.author,
                articleStore.currentArticle.title,
                articleStore.currentArticle.imageURL,
                articleStore.currentArticle.content,
                articleStore.currentArticle.important,
                articleStore.currentArticle.category);
        }).catch(error => {
            console.log(error)
        });
    }

    submitArticle() {
        if (super.submitArticle()){
            super.updateInternalValues();

            let editedArticle = super.createArticle(this.props.match.params.article_id);

            articleStore.editArticle(editedArticle).then(response => {
                if (response.statusText === 'OK') {
                    history.push("/articles/article/" + editedArticle.id);
                }
            }).catch(error => console.log(error));
        }
        return true;

    }
}