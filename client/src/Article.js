//@flow
export class Article{
    static constantURL:string = 'https://png.pngtree.com/png-clipart/20190515/original/pngtree-coffee-time-png-image_3626459.jpg';
    id: number;
    author: string;
    title: string;
    imageURL: string;
    time: string;
    content: string;
    category: number;
    important: boolean;
    constantURL: string = 'https://png.pngtree.com/png-clipart/20190515/original/pngtree-coffee-time-png-image_3626459.jpg';
    comments: Comment[];
    constructor(id: number, author: string, title: string, imageURL: string, time: string, content: string, category: number, important: boolean){
        this.id = id;
        this.author = author;
        this.title = title;
        if (imageURL == null){
            this.imageURL = this.constantURL;
        }
        else{
            this.imageURL = imageURL;
        }
        this.time = time;
        this.content = content;
        this.category = category;
        this.important = important;
        this.comments = [];
    }

    addComments(comments: Comment[]){
        if (comments){
            comments.map(element => this.comments.push(element));
        }
    }
};