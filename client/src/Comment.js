//@flow
export class Comment{
    id: number;
    author: string;
    content: string;
    articles_id: number;

    constructor(id: number, author: string, content: string, article_id: number) {
        this.id = id;
        this.author = author;
        this.content = content;
        this.articles_id = article_id;
    }
};