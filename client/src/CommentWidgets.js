//@flow
import {Component} from "react-simplified";
import {articleStore} from "./stores";
import {Alert, Button, Card, Column, Row} from "./widgets";
import * as React from "react";
import {Comment} from './Comment';

export class SingleComment extends Component<{ comment: Comment }> {
    render() {
        return (
            <Card title={this.props.comment.author}>
                <Row>
                    <Column>
                        <div className='card-text'>{this.props.comment.content}</div>
                    </Column>
                    <Column>
                        <Button.Danger onClick={() => this.deleteComment()}>Delete comment</Button.Danger>
                    </Column>
                </Row>
            </Card>
        );
    }

    deleteComment() {
        articleStore.deleteComment(this.props.comment.id).then(articleStore.getArticle(articleStore.currentArticle.id));
    }
}

export class AddComment extends Component<{ article_id: number }> {
    author: string = '';
    content: string = '';

    render() {
        return (
            <Card title='Add Comment'>
                <form>
                    <Row>
                        <Column width={2}><span className='input-group-text'>Username</span></Column>
                        <Column>
                            <input type='text'
                                   value={this.author}
                                   onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.author = event.target.value)}
                                   className='form-control' aria-label='Large' aria-describedby="inputGroup-sizing-sm"/>
                        </Column>
                    </Row>
                    <Row>
                        <Column width={2}><span className='input-group-text'>Comment</span></Column>
                        <Column>
                            <input type='text'
                                   value={this.content}
                                   onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.content = event.target.value)}
                                   className='form-control' aria-label='Large' aria-describedby="inputGroup-sizing-sm"/>
                        </Column>
                    </Row>
                    <Button.Info onClick={this.addComment}>Add comment</Button.Info>
                </form>
            </Card>
        );
    }

    addComment() {
        let errors: string[] = [];
        if (this.author === undefined || this.author.length === 0){
            errors.push("Husk brukernavn");
        }
        if (this.content === undefined || this.content.length === 0){
            errors.push("Husk innhold");
        }
        if (errors.length > 0){
            Alert.danger(errors.reduce((acc, e) => (acc + e + " | "), ""));
        }
        else{
            let id: number = Math.max(...articleStore.currentArticle.comments.map(e => e.id)) + 1;
            if (id < 0) {
                id = 0;
            }
            let comment: Comment = new Comment(id, this.author, this.content, articleStore.currentArticle.id);
            console.log(comment);
            this.author = '';
            this.content = '';
            articleStore.postComment(comment).then(() => articleStore.getArticle(articleStore.currentArticle.id));
        }
    }
}